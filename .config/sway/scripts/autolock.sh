swayidle -w \
	timeout 600 '~/.config/hypr/scripts/lock.sh' \
	timeout 1200 'swaymsg "output * dpms off"' \
	resume 'swaymsg "output * dpms on"' \
	before-sleep '~/.config/hypr/scripts/lock.sh'
