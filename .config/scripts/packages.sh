#! /usr/bin/env bash

sudo pacman -S --needed base-devel

sudo pacman -S git clang neovim curl \
	iwd networkmanager fd ripgrep fzf tmux hyprpaper \
	nodejs npm python python-pip go kitty alacritty wlroots zsh \
	kvantum qt6ct qt6ct waybar rofi xterm wayland wayland-utils wl-mirror wev \
	v4l2loopback-dkms v4l-utils tree-sitter tree-sitter-cli qt6-wayland \
	qt5-wayland polkit polkit-kde-agent polkit-gnome pam pamixer pambase openssh nm-connection-editor \
	ninja lxappearance linux-zen linux-zen-headers linux-headers hyprpicker gtk3 \
	gtk2 starship dolphin dkms dbus bluedevil base figlet zathura \
	autorandr arandr network-manager-applet ncurses figlet lf brightnessctl kdeconnect pavucontrol \
	noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra
git clone https://github.com/morganamilo/paru.git

cd ~/paru || echo

makepkg -si


paru -S swayidle swaylock hyprland-git \
	unzip  hyprpicker brave-bin hyprwayland-scanner hyprshot \
	ttf-apple-emoji ttf-nerd-fonts-symbols-common ttf-jetbrains-mono-nerd \
	nwg-look-bin redshift-wayland-git \
	dracula-gtk-theme dracula-icons-git wayclip cliphist \
	hyprpicker tmuxinator bibata-cursor-theme

cd ~ || echo

sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

git clone https://github.com/zsh-users/zsh-autosuggestions "${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions"


