#!/bin/bash

function run {
	if ! pgrep $1; then
		$@ &
	fi
}
#run xrandr --output VGA-1 --primary --mode 1360x768 --pos 0x0 --rotate normal
#run xrandr --output HDMI2 --mode 1920x1080 --pos 1920x0 --rotate normal --output HDMI1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output VIRTUAL1 --off
run picom -b
run nm-applet
run pamac-tray
run blueberry-tray
run /usr/lib/polkit-gnome
run /usr/bin/kdeconnect-indicator
run xinput set-prop 10 317 1
run zsh -c "tmuxifier load-session notes"
run xfce4-clipman
