dir="$HOME/.config/rofi/launchers/type-7"
theme='style-7'

if pidof rofi;then

	echo
else
	selected="$(cat <$HOME/projects.txt | rofi -dmenu -theme ${dir}/${theme}.rasi)"
	$HOME/.config/hypr/scripts/projectr/run_or_exit_nvim.sh "$selected"
fi




