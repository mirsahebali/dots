eval "$(oh-my-posh init zsh --config ~/.omp/amro.omp.json)"
# eval "$(glow completion zsh)"
export SSD_PATH=/run/media/ssd/
export ZSH="$HOME/.oh-my-zsh"
export QT_QPA_PLATFORM=wayland
export ANDROID_HOME=$HOME/Android/Sdk
export HISTCONTROL=ignoreboth:erasedups
export MANPATH="/usr/local/man:$MANPATH"
export MANPATH="/usr/local/share/man:$MANPATH"
export MANPATH="/usr/share/man/:$MANPATH"
export LANG=en_US.UTF-8
export EDITOR="nvim"
export ARCHFLAGS="-arch x86_64"
export NVM_DIR="$HOME/.nvm"
export GOPATH="$HOME/go"
export GOBIN="$HOME/go/bin"
export BUN_INSTALL="$HOME/.bun"
export JAVA_HOME=/opt/android-studio/jbr
export ANDROID_HOME="$HOME/Android/Sdk"
export NDK_HOME="$ANDROID_HOME/ndk/28.0.12433566/"
export ANDROID_NDK="$ANDROID_HOME/ndk/28.0.12433566/"
# export PATH="/usr/lib/jvm/java-21-openjdk/bin/:$PATH"

export FLYCTL_INSTALL="/home/saheb/.fly"
export PATH="$FLYCTL_INSTALL/bin:$PATH"
export PATH=$HOME/bin:/usr/local/bin:$PATH
export PATH="$HOME/.local/bin/":$PATH
export PATH="$HOME/bin/":$PATH
export PATH="/usr/bin/":$PATH
export PATH=$PATH:$HOME/.bin
export PATH="$HOME/.config/tmux/plugins/tmuxifier/bin:$PATH"
export PATH="$BUN_INSTALL/bin:$PATH"
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/go/bin
export PATH=$PATH:$HOME/go/bin
export PATH=$PATH:$HOME/.ghcup/bin
export PATH=$PATH:$HOME/.config/emacs/bin
export PATH=$PATH:$HOME/.local/share/gem/ruby/3.2.0/bin
export PATH="$PATH:$HOME/.tmuxifier/bin"

# Turso
export PATH="$PATH:/home/saheb/.turso"

export DENO_INSTALL="$HOME/.deno"
export PATH="$DENO_INSTALL/bin:$PATH"
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[01;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'

# # Initialization code that may require console input (password prompts, [y/n]
# # confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

# zinit package manager
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

if [ ! -d "$ZINIT_HOME" ]; then
	mkdir -p "$(dirname  $ZINIT_HOME)"
	git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi


source "$ZINIT_HOME/zinit.zsh"

zinit ice depth=1;
# zinit light romkatv/powerlevel10k

# plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light Aloxaf/fzf-tab

# auto load completions
autoload -Uz compinit && compinit


# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
# [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

bindkey -e
bindkey '^p' history-search-backward
bindkey '^n' history-search-forward
bindkey '^g' forward-word
autoload -z edit-command-line
zle -N edit-command-line
bindkey "^O" edit-command-line

# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt hist_ignore_space
setopt sharehistory
setopt hist_ignore_dups
setopt hist_save_no_dups


zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' list-colors '${(s.:.)LS_COLORS}'
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'

zinit snippet OMZP::git
zinit snippet OMZP::npm
zinit snippet OMZP::copypath
zinit snippet OMZP::copyfile
zinit snippet OMZP::dirhistory
zinit snippet OMZP::command-not-found
zinit snippet OMZP::golang
zinit snippet OMZP::man
zinit snippet OMZP::zoxide

eval "$(fzf --zsh)"
eval "$(zoxide init --cmd c zsh)"


alias ls="ls --color"
alias lsd="lsd -a1"
alias dots="cd ~/.dotfiles/ && vim ."
alias fzf="fzf --preview='bat {}'"
alias ll="ls -lah"
alias la="ls -a"
alias ..="cd .."
alias ...="cd ../../"
alias ....="cd ../../../"
alias _="sudo"
alias tmuxconf="cd ~/.config/tmux/ && nvim tmux.conf"
alias ta='tmux a'
alias kittyconf="cd ~/.config/kitty/ && nvim kitty.conf"
alias viconf='cd ~/.config/nvim/ && nvim'
alias pb="~/pocketbase serve"
alias alacrittyconf="cd ~/.config/alacritty && nvim"
alias vim=/usr/bin/nvim
alias vi=/usr/bin/vim
alias vi.="/usr/bin/vim ."
alias vim.="/usr/bin/nvim ."
alias qemu=qemu-system-x86_64
icat(){
  echo kitty +kitten icat "$@"
  kitty +kitten icat "$@"
}
alias :q=exit
alias :w=touch
alias hconf="~/.config/hypr && nvim ."
alias air=~/go/bin/air
alias awconf="~/.config/awesome/ && nvim rc.lua"
alias docker="sudo docker"
alias dockerd="sudo dockerd"
alias zconf="nvim ~/.zshrc"
alias zc="nvim ~/.zshrc"
alias hc="nvim ~/.config/hypr/hyprland.conf"
alias wpconf="alacritty -e nvim ~/.config/hypr/hyprpaper.conf"
alias doom_emacs="emacs --init-directory=~/fun/emacs/"
alias update_all_dots="~/dots/scripts/update.sh"
alias update_doom="~/dots/scripts/update_doom.sh"
alias update_nvim="~/dots/scripts/update_nvim.sh"
alias update_emacs="~/dots/scripts/update_emacs.sh"
alias update_dots="~/dots/scripts/update_dots.sh"
alias rr="ranger"
alias hx="helix"
alias yy="yazi"
alias code="vscodium"
alias sch="glow ~/SCHEDULE.md"
zz(){
  zathura "$@" &
}
alias c="z"
alias l="ls -latrh"
alias ll="ls -lah"
alias noice="$EDITOR "
alias cls="clear"
alias onlyoffice="onlyoffice-desktopeditors "

fwhich(){
  file $(which $1)
}
figlol(){
  figlet "$1" | lolcat
}
dnd(){
    set -x
    killall kdeconnect-indicator
}
dnda(){
    set -x
    killall kdeconnect-indicator dunst
}
enable_notification(){
    set -x
    setsid /usr/bin/kdeconnect-indicator &
    setsid /usr/bin/dunst &
}
# Alias functions
rs_waybar(){
  ((killall waybar || waybar &) && waybar &) && ((killall waybar || waybar &) && waybar &)
}
mdr(){
 mkdir "$1" && cd "$1"
}
countdown() {
    dunstify Timer Started
    start="$(( $(date '+%s') + $1))"
    while [ $start -ge $(date +%s) ]; do
        time="$(( $start - $(date +%s) ))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
    dunstify Timer Ended
}

stopwatch() {
    start=$(date +%s)
    while true; do
        time="$(( $(date +%s) - $start))"
        printf '%s\r' "$(date -u -d "@$time" +%H:%M:%S)"
        sleep 0.1
    done
}
cfmt(){
  echo ~/.local/share/nvim/mason/bin/clang-format --style $1 --dump-config > .clang-format
  ~/.local/share/nvim/mason/bin/clang-format --style $1 --dump-config > .clang-format
}
co(){
  if [[ -f "$1.cpp" ]]; then
    echo "g++ $1.cpp -o $1 && sleep 1 && ./$1";
           g++ $1.cpp -o $1 && sleep 1 && ./$1
  else
    echo "gcc $1.c -o $1 && sleep 1 && ./$1";
          gcc $1.c -o $1 && sleep 1 && ./$1;
  fi
}

connect_boult(){
  bluetoothctl connect $(bluetoothctl devices | grep "Boult Audio Airbass" | awk '{print $2}')
}

disconnect_boult(){
  bluetoothctl disconnect $(bluetoothctl devices | grep "Boult Audio Airbass" | awk '{print $2}')
}


[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# opam configuration
[[ ! -r $HOME/.opam/opam-init/init.zsh ]] || source $HOME/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

[ -f "$HOME/.ghcup/env" ] && . "$HOME/.ghcup/env" # ghcup-env

if [ -f "$(which tmuxifier)" ]; then
  eval "$(tmuxifier init -)"
fi

# bun completions
[ -s "/home/saheb/.bun/_bun" ] && source "/home/saheb/.bun/_bun"

source /usr/share/nvm/init-nvm.sh
