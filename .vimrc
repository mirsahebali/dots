
set relativenumber
" Switch to normal from insert
imap jk <esc>
imap JK <esc>
" Quit
nmap <space>qq <cmd>q<cr>
" Save file
nmap <space>fs <cmd>w<cr>
" Split window movement
nmap <space>w <C-w>

nmap <space>bd <cmd>bdelete<cr>
nmap - <cmd>Ex<cr>
nmap <space><space> <cmd>Ex<cr>
nmap <space>. <cmd>Ex<cr>
nmap <space>sf <cmd>FZF<cr>
nmap j gj
nmap k gk
imap <tab> <space><space>
nmap <tab> 0i<tab><esc>
nmap <space>bd :bdelete<cr>
imap <c-e> <esc>A
imap <c-a> <esc>I
syntax on
filetype on
set tabstop=2
set laststatus=2
filetype plugin indent on
set expandtab
set autoindent
set smartindent
nmap <space>ss <cmd>term<cr>
syntax enable



let data_dir = has('vim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif



call plug#begin()

" List your plugins here
Plug 'tpope/vim-sensible'

nmap <space>ff :FZF<cr>


call plug#end()

